/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';
import { Provider } from 'react-redux'
import configureStore from './src/services/redux/store'
import { PersistGate } from 'redux-persist/integration/react';

import NavCountainer from './src/services/navigation';


const store = configureStore().store;

let persistor = configureStore().persistor;

const App: () => React$Node = () => {
  return (
    <Provider store={store}>
      <NavCountainer></NavCountainer>
    </Provider>
  );
};


export default App;
