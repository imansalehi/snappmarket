import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from '../../Features/Home';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar, TouchableOpacity
} from 'react-native';
import MyTabBar from '../../Features/Components/TabBar';
const Stack = createStackNavigator();
function SettingsScreen() {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text>otherpages whish is not required in Test</Text>
    </View>
  );
}

const Tab = createBottomTabNavigator();

function NavCountainer() {
  return (

    //TO DO iman: we can Nest it with tab navigator whenever internal pages created <<<<<<<<<<<<<<<<<
    //   <NavigationContainer>
    //     <Stack.Navigator   screenOptions={{
    //   headerShown: false
    // }}>
    //       <Stack.Screen  name="Home" component={HomeScreen} />
    //     </Stack.Navigator>
    //   </NavigationContainer>

    <NavigationContainer>
      <Tab.Navigator tabBar={props =>  <MyTabBar {...props} />}>
         
        <Tab.Screen options={{title:'خانه',iconName:'home'}} name="Home" component={HomeScreen} />
        <Tab.Screen options={{title:'لیست شما',iconName:'list'}} name="List" component={SettingsScreen} />
        <Tab.Screen options={{title:'جستوجو',iconName:'search'}} name="search" component={SettingsScreen} />
        <Tab.Screen options={{title:'دسته بندی',iconName:"grid-outline",ionic:true}} name="Catagories" component={SettingsScreen} />
        <Tab.Screen options={{title:'سبد خرید',iconName:"cart-outline",ionic:true}} name="Settings" component={SettingsScreen} />
      </Tab.Navigator>
    </NavigationContainer>
  );
}




export default NavCountainer;