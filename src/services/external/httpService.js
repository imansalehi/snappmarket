import { apiConfig } from '../external/restConfig'
export default class RestFetch {

    constructor() {
        this.root = apiConfig().apiRoot
        this.Bearer = 'Token '
    }

    _send(url, body, auth, requestType) {


        var RequestType = requestType ? requestType : "POST"

        var Headers = auth ? {
            "Content-Type": "application/json; charset=utf-8",
            'Authorization': this.Bearer + auth,
        } : {
                "Content-Type": "application/json; charset=utf-8",
                'Accept': 'application/json',
            }

        return Promise.race([
            new Promise((resolve, reject) => {
                  
                        fetch(this.root + url, {
                            method: RequestType,
                            headers: Headers,
                            body: JSON.stringify(body)
                        }).then(response => {
                           
                            return response.json().then((res) => {
                                return { response: res, ok: response.ok, status: response.status }
                            }).catch((er) => {
                                return { response: response, ok: false }
                            })

                        }).then((res) => {
                            res.ok == true ?
                                resolve(res.response) :
                                reject(res.response)
                        }).catch((error) => {
                           
                            reject(error)
                        });
                    

            }),

            new Promise((_, reject) =>
                setTimeout(() => {
                    reject({ message: 'اشکال در برقراری ارتباط با سرور ',status:99 })
                }, 60000)
            )

        ])

    }


  

}

