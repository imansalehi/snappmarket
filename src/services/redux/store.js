import { createStore, combineReducers,applyMiddleware } from 'redux';
import counterReducer from './reducers/counterReducer';
import { persistStore, persistReducer } from 'redux-persist'
import AsyncStorage from '@react-native-community/async-storage';
import thunk from 'redux-thunk';
import productsReducer from './reducers/productsReducer';

const rootReducer = combineReducers({
  counter: counterReducer,// for testing
  vendors:productsReducer //<<<< set products in SnappMarket Vendors
});

const persistConfig = {
  timeout: 0,
    key: 'root',
    storage:AsyncStorage,
    whiteliast:['vendors'],
    blacklist: ['counter'] 
  }
  
  const persistedReducer = persistReducer(persistConfig, rootReducer)
  
  export default  () => {
    let store = createStore(persistedReducer,  applyMiddleware(thunk))
    let persistor = persistStore(store)
    return { store, persistor }
  }
