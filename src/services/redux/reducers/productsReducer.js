import { LOAD_PRODUCTS } from '../actions/constants'

const initialState = {
    products: null
}

const productsReducer = (state = initialState, action) => {

    switch (action.type) {
        case LOAD_PRODUCTS:
            return {
                ...state,
                products: action.payload
            }

        default: return state
    }
}


export default productsReducer
