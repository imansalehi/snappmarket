export default class Formatter {


    commaSeparateNumber(x) {
        if (!x) x = 0;
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

}
