
import React, { useState } from 'react';

import { View, FlatList, StyleSheet, Text, TouchableOpacity, Image, Dimensions } from 'react-native'
import CountDownComponent from '../Components/countDowns';
import Label from '../Components/Label';
import Formatter from '../../services/utils/Formatter';
import { grayColor, greenColor, yellowColor } from '../../services/utils/theme';
import OffLabel from '../Components/offLable';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/FontAwesome5';
const { width, height } = Dimensions.get('window');
const DATA = [
    {
        id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
        title: 'First Item',
    },
    {
        id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
        title: 'Second Item',
    },
    {
        id: '58694a0f-3da1-471f-bd96-145571e29d72',
        title: 'Third Item',
    },
];
const _F = new Formatter()

const Item = ({ item, last }) => (

    <View style={styles.item}>
        <View style={{ flex: 1, position: 'relative' }}>
            <Image style={{ flex: 1 }} source={{ uri: item.images[0].thumb }} />
            <View style={{ position: 'absolute', bottom: 0, right: 0 }}>
                <OffLabel style={styles.off} value={item.discount_percent} />
            </View>
        </View>
        <View style={{ flex: .5, justifyContent: 'space-between' }}>
            <Label style={styles.title}>{item.title}</Label>

            <Label fontStyle='Bold' style={styles.price}>{`${_F.commaSeparateNumber(item.price)} تومان`}</Label>

            <Label style={styles.offText}>{`${_F.commaSeparateNumber(item.discounted_price)}`}</Label>
        </View>


        <View style={{ position: 'absolute', top: 10, left: 10 }}>
            <Icon name={'plus-circle'} color={'green'} size={20} />
        </View>

        {last == true && <View style={{ position: 'absolute', left: 0, right: 0, top: 0, bottom: 0, justifyContent: 'center', backgroundColor: 'white', opacity: .5, zIndex: 2 }}>


        </View>}

        {last == true && <View style={{ position: 'absolute', left: 0, right: 0, top: 0, bottom: 0, justifyContent: 'center', zIndex: 3, alignItems: 'center' }}>
            <Icon2 name={'chevron-circle-left'} color={greenColor} size={50} />

        </View>}

    </View>
);
const ItemList = (props) => {

    const renderItem = ({ item, index }) => (

        <Item item={item} last={(index + 1) == props.data.products.length ? true : false} />

    );



    return (
        <View style={{ backgroundColor: 'white', elevation: 5, position: 'relative' }}>
            <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'space-between', padding: 12 }}>
                <TouchableOpacity onPress={() => null} style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Icon name={'chevron-left'} color={'blue'} size={8} />
                    <Label style={{ fontSize: 15, color: 'blue' }}>بیشتر</Label>

                </TouchableOpacity>
                <Label fontStyle="Bold" style={{ fontSize: 15 }}>{props.data.title}</Label>
            </View>
            <FlatList
                data={props.data.products}
                renderItem={renderItem}
                keyExtractor={item => item.id}
                horizontal={true}
                inverted={true}

            />

            {/* <View style={{position:'absolute',left:0,top:0,bottom:0,justifyContent:'center',backgroundColor:'white',opacity:.5,zIndex:2}}>
            <Icon2 name={'chevron-circle-left'} color={greenColor} size={50} />
           
            </View> */}

        </View>
    );


}


export default ItemList;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        //  marginTop: StatusBar.currentHeight || 0,
    },
    item: {
        backgroundColor: 'white',
        padding: 10,
        paddingBottom: 1,
        marginVertical: 8,
        marginHorizontal: 16,
        width: width * .35,
        height: 230,
        position: 'relative',
        borderRadius: 5

    },
    title: {
        fontSize: 10,
        color: grayColor
    },

    price: {
        fontSize: 10,
        color: 'black'
    },
    offText: {
        fontSize: 8,
        textDecorationLine: 'line-through',
        color: grayColor
    },
    off: {
       borderRadius:5
    }
});
