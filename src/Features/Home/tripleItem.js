
import React, { useState } from 'react';

import { View, FlatList, StyleSheet, Text, TouchableOpacity, Image, Dimensions } from 'react-native'
import CountDownComponent from '../Components/countDowns';
import Label from '../Components/Label';
import Formatter from '../../services/utils/Formatter';
import { grayColor, greenColor, yellowColor, TopTextColor } from '../../services/utils/theme';
import OffLabel from '../Components/offLable';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/FontAwesome5';
const { width, height } = Dimensions.get('window');

const TripleList = (props) => {

    const renderItem = () => {
        return props.data.images.map((item) => {
         
            return (
            <View style={{ flexBasis: '33%',padding:5 }}>
               <View style={{backgroundColor:'white',elevation:5,borderRadius:15,flex:1,padding:5,
               alignItems:'center',justifyContent:'center'}}>
                   <Image source={{uri:item.img_url}} style={{width:width*.3,height:width*.3}}/>
               </View>
            
             
            </View>
            )
        })
        
    }
    return (
        <View style={{
            justifyContent: 'flex-start',
            flexDirection: 'row',
            flexWrap: 'wrap',
            flex: 1,
        }}>
            <View style={{width:'100%',alignItems:'center'}}>
                <Label fontStyle='Bold' style={{color:TopTextColor,fontSize:20}}>
                    {props.data.title}
                </Label>
            </View>

            {renderItem()}



        </View>
    );


}


export default TripleList;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        //  marginTop: StatusBar.currentHeight || 0,
    },
    item: {

        paddingBottom: 1,
        marginHorizontal: 16,
        width: 300,
        height: 150,
        position: 'relative',
        borderRadius: 5

    },
    title: {
        fontSize: 10,
        color: grayColor
    },

    price: {
        fontSize: 10,
        color: 'black'
    },
    offText: {
        fontSize: 8,
        textDecorationLine: 'line-through',
        color: grayColor
    },
    off: {
        borderRadius: 5
    }
});
