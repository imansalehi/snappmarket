
import React, { useState } from 'react';

import { View, FlatList, StyleSheet, Text, TouchableOpacity, Image, Dimensions } from 'react-native'
import CountDownComponent from '../Components/countDowns';
import Label from '../Components/Label';
import Formatter from '../../services/utils/Formatter';
import { grayColor, greenColor, yellowColor } from '../../services/utils/theme';
import OffLabel from '../Components/offLable';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/FontAwesome5';
const { width, height } = Dimensions.get('window');
const DATA = [
    {
        id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
        title: 'First Item',
    },
    {
        id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
        title: 'Second Item',
    },
    {
        id: '58694a0f-3da1-471f-bd96-145571e29d72',
        title: 'Third Item',
    },
];
const _F = new Formatter()

const Item = ({ item, last }) => (

    <View style={styles.item}>
       
            <Image style={{ flex: 1,borderRadius:15 }} source={{ uri: item.img_url }} />

    </View>
);
const RawList = (props) => {

    const renderItem = ({ item, index }) => (

        <Item item={item} />

    );



    return (
        <View>
            <FlatList
                data={props.data}
                renderItem={renderItem}
                keyExtractor={item => item.id}
                horizontal={true}
                inverted={true}

            />

        

        </View>
    );


}


export default RawList;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        //  marginTop: StatusBar.currentHeight || 0,
    },
    item: {
        
        paddingBottom: 1,
        marginHorizontal: 16,
        width: 300,
        height: 150,
        position: 'relative',
        borderRadius: 5

    },
    title: {
        fontSize: 10,
        color: grayColor
    },

    price: {
        fontSize: 10,
        color: 'black'
    },
    offText: {
        fontSize: 8,
        textDecorationLine: 'line-through',
        color: grayColor
    },
    off: {
       borderRadius:5
    }
});
