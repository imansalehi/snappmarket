import React, { useEffect } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    Image, TouchableOpacity, TextInput, Dimensions, Animated
} from 'react-native';
import { INCREAS } from '../../services/redux/actions/counter'
import { loadProducts } from '../../services/redux/actions/products'
import { connect } from 'react-redux'
import products from '../../business/products'
import Label from '../Components/Label';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { headerBGColor, grayColor } from '../../services/utils/theme'
import banner from '../../assets/images/banner.jpg';

import OffList from './offList';
import ItemList from './itemList';
import RawList from './rawList';
import WrapList from './wrapList';
const { width, height } = Dimensions.get('window');
class HomeScreen extends React.Component {
    constructor(props) {
        super(props)
        this._products = new products()
        this.state = {
            offTime: null,
            scrlY: new Animated.Value(0)
        }
    }

    componentDidMount() {
        this._products.getProducts().then((res) => {
            //  alert(new Date(res.items[16].expire_time.replace(' ','T'))-new Date() )
            this.setState({ offTime: new Date(res.items[16].expire_time.replace(' ', 'T')) - new Date() })
            this.props.LOAD(res)
            // alert(this.props.Products.items[0].Images[0])
        }).catch((e) => {
            alert(e)
        })

        //  console.log('Pruds:', this.props.Products.items[0].images[0].img_url)

    }


    render() {
        const itemOpacity = this.state.scrlY.interpolate({
            inputRange: [0, 150],
            outputRange: [0, 1],
            extrapolate: 'clamp',
        });

       const itemHeight=  this.state.scrlY.interpolate({
            inputRange: [0, 150],
            outputRange: [0, 60],
            extrapolate: 'clamp',
        });
        if (this.props.Products != null) {
            return (
                <SafeAreaView>
                    <Animated.View style={{ position: 'absolute', top: 20, left: 20, right: 20, zIndex: 10,
                    alignItems:'center', opacity: itemOpacity }}>
                          <Animated.View
                                        style={{
                                            borderColor: grayColor,
                                            borderWidth: 1, width: '100%',backgroundColor:'white',
                                            borderRadius: 5, padding: 10, 
                                            paddingLeft: 20, paddingRight: 20, 
                                            flexDirection: 'row', justifyContent: 'space-between',
                                            alignItems:'center',
                                            height:itemHeight
                                        }}>
                                        <Icon name={'search'} color={grayColor} size={20} />
                                        <Animated.Text style={{color: grayColor,fontFamily:'IRANSansMobile(FaNum)'}}>
                                        جستجوی محصول یا نام برند
                                        </Animated.Text>
                                       
                                        <Icon name={'bars'} color={grayColor} size={20} />
                                    </Animated.View>
                    </Animated.View>
                    <ScrollView
                        onScroll={Animated.event(
                            [{ nativeEvent: { contentOffset: { y: this.state.scrlY } } }]
                        )}
                        contentInsetAdjustmentBehavior="automatic">
                        <View style={{ padding: 5, paddingRight: 15, width: '100%', backgroundColor: headerBGColor, flexDirection: 'row', justifyContent: 'flex-end' }}>
                            <View style={{ marginRight: 12 }}>
                                <Label style={{ color: grayColor }}>آدرس شما:</Label>
                                <Label fontStyle='Bold'>{`تهران، میدان انقلاب`}</Label>
                            </View>

                            <View style={{ justifyContent: 'center' }}>
                                <TouchableOpacity onPress={() => null}>
                                    <Icon name={'bars'} color={'#222'} size={20} />
                                </TouchableOpacity>
                            </View>


                        </View>


                        <View style={{ width: '100%', height: height * .2, position: 'relative', overflow: 'visible' }}>
                            <Image source={banner} style={{ height: height * .2 }}></Image>
                            <View style={{ position: 'absolute', right: 0, left: 0, top: '30%', padding: 20, alignItems: 'center', zIndex: 1 }}>
                                <View style={{ backgroundColor: 'white', width: '95%', alignItems: 'center', padding: 20, elevation: 5, borderRadius: 10 }}>
                                    <View style={{ width: '100%', justifyContent: 'space-between', flexDirection: 'row', marginBottom: 30 }}>

                                        <Label style={{ color: grayColor }}>تغییر فروشگاه</Label>

                                        <Label fontStyle='Bold'>هایپر استار صبا</Label>

                                    </View>
                                    <View style={{ width: '100%', justifyContent: 'space-between', flexDirection: 'row', marginBottom: 30 }}>
                                        <View style={{ flex: 1, justifyContent: 'flex-end', flexDirection: 'row', alignItems: 'center' }}>
                                            <Label style={{ fontSize: 10, marginRight: 5 }}>{`ارسال رایگان بالای 100 هزار تومان `}</Label>
                                            <Icon name={'motorcycle'} color={'black'} size={12} />
                                        </View>
                                        <View style={{ flex: 1, justifyContent: 'flex-end', flexDirection: 'row', alignItems: 'center' }}>
                                            <Label style={{ fontSize: 10, marginRight: 5 }}>{`تحویل از امروز ساعت 18:00 `}</Label>
                                            <Icon name={'clock'} color={'black'} size={12} />
                                        </View>
                                    </View>
                                    <TouchableOpacity
                                        style={{
                                            borderColor: grayColor,
                                            borderWidth: 1, width: '100%',
                                            borderRadius: 10, padding: 10, paddingLeft: 20, paddingRight: 20, flexDirection: 'row', justifyContent: 'space-between'
                                        }}>
                                        <Icon name={'search'} color={grayColor} size={20} />
                                        <Label style={{ color: grayColor }}>جستجوی محصول یا نام برند</Label>
                                    </TouchableOpacity>

                                </View>
                            </View>
                        </View>

                        <View style={{ width: '100%', position: 'relative', overflow: 'visible', marginTop: '40%', padding: 15 }}>
                            <Image source={{ uri: this.props.Products.items[0].images[0].img_url }}
                                style={{ height: height*.2, borderRadius: 15, resizeMode: 'stretch' }}></Image>
                        </View>

                        <View style={{ width: '100%', position: 'relative', overflow: 'visible', padding: 15 }}>
                            <Image source={{ uri: this.props.Products.items[3].images[0].img_url }}
                                style={{ height: height*.2, borderRadius: 15, resizeMode: 'stretch' }}></Image>
                        </View>

                        <View style={{ width: '100%', marginBottom: 10 }}>
                            <RawList data={this.props.Products.items[4].images}/>

                        </View>

                        <View style={{ width: '100%', marginBottom: 10 }}>
                           <WrapList data={this.props.Products.items[5]}/>

                        </View>

                        <View style={{ width: '100%', position: 'relative', overflow: 'visible', padding: 15 }}>
                            <Image source={{ uri: this.props.Products.items[10].images[0].img_url }}
                                style={{ height: height*.2, borderRadius: 15, resizeMode: 'stretch' }}></Image>
                        </View>

                        <View style={{ width: '100%', marginBottom: 10 }}>
                           <WrapList data={this.props.Products.items[11]} regular/>

                        </View>

                        <View style={{ width: '100%', position: 'relative', overflow: 'visible', marginTop: 10, padding: 15 }}>
                            <Image source={{ uri: this.props.Products.items[12].images[0].img_url }}
                                style={{ height: height * .2, borderRadius: 15, resizeMode: 'stretch' }}></Image>
                        </View>

                        <View style={{ width: '100%', position: 'relative', overflow: 'visible', marginTop: 10, padding: 15 }}>
                            <Image source={{ uri: this.props.Products.items[14].images[0].img_url }}
                                style={{ height: height * .15, borderRadius: 15, resizeMode: 'stretch' }}></Image>
                        </View>
                     
                        <View style={{ width: '100%', marginBottom: 10 }}>
                            <OffList data={this.props.Products.items[16].products} until={Math.floor((new Date(this.props.Products.items[16].expire_time.replace(' ', 'T')) - new Date()))} />

                        </View>

                        <View style={{ width: '100%' }}>
                            <ItemList data={this.props.Products.items[17]} />

                        </View>

                        <View style={{ width: '100%', position: 'relative', overflow: 'visible', marginTop: 10, padding: 15 }}>
                            <Image source={{ uri: this.props.Products.items[19].images[0].img_url }}
                                style={{ height: height * .2, borderRadius: 15, resizeMode: 'stretch' }}></Image>
                        </View>

                        <View style={{ width: '100%' }}>
                            <ItemList data={this.props.Products.items[20]} />

                        </View>


                    </ScrollView>
                </SafeAreaView>

            )
        } else {
            return (null)
        }
    }
}

const mapStateToProps = (state) => {
    // console.log('state is :',state.vendors)
    return {
        Counter: state.counter.count,
        Products: state.vendors.products
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        INC: () => {
            dispatch(INCREAS())
        },
        LOAD: (val) => {
            dispatch(loadProducts(val))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen); 