
import React, { useState } from 'react';

import { View, FlatList, StyleSheet, Text, Animated, Image, Dimensions } from 'react-native'
import CountDownComponent from '../Components/countDowns';
import Label from '../Components/Label';
import Formatter from '../../services/utils/Formatter';
import { grayColor, greenColor, yellowColor } from '../../services/utils/theme';
import OffLabel from '../Components/offLable';
import Icon from 'react-native-vector-icons/FontAwesome';
const { width, height } = Dimensions.get('window');
const DATA = [
    {
        id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
        title: 'First Item',
    },
    {
        id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
        title: 'Second Item',
    },
    {
        id: '58694a0f-3da1-471f-bd96-145571e29d72',
        title: 'Third Item',
    },
];
const _F = new Formatter()

const Item = ({ item }) => (
    <View style={styles.item}>
        <View style={{ flex: 1 }}>
            <Image style={{ flex: 1 }} source={{ uri: item.images[0].thumb }} />
        </View>
        <View style={{ flex: .5, justifyContent: 'space-between' }}>
            <Label style={styles.title}>{item.title}</Label>

            <Label fontStyle='Bold' style={styles.price}>{`${_F.commaSeparateNumber(item.price)} تومان`}</Label>

            <Label style={styles.offText}>{`${_F.commaSeparateNumber(item.discounted_price)}`}</Label>
        </View>
        <View style={{ position: 'absolute', bottom: 0, left: 0 }}>
            <OffLabel style={styles.off} value={item.discount_percent} />
        </View>

        <View style={{ position: 'absolute', top: 10, left: 10 }}>
            <Icon name={'plus-circle'} color={'green'} size={20} />
        </View>
    </View>
);
const OffList = (props) => {
    const [scrlY, setScrlY] = useState(new Animated.Value(1));
    const renderItem = ({ item }) => (
        <Item item={item} />
    );
    const itemOpacity = scrlY.interpolate({
        inputRange: [0, 160],
        outputRange: [1, 0],
        extrapolate: 'clamp',
    });


    return (
        <View style={{ backgroundColor: yellowColor, position: 'relative' }}>
            <FlatList
                data={props.data}
                renderItem={renderItem}
                keyExtractor={item => item.id}
                horizontal={true}
                inverted={true}
                contentContainerStyle={{ paddingStart: 200 }}
                onScroll={Animated.event(
                    [{ nativeEvent: { contentOffset: { x: scrlY } } }]
                )}

            />
            <Animated.View style={{
                position: 'absolute', right: 0, top: 0,
                bottom: 0, justifyContent:'center',
                alignContent: 'center', zIndex: -1,
                opacity: itemOpacity,padding:20,
            }}>
                <View style={{alignItems:'center'}}>
                <Label fontStyle={'Bold'} style={{color:'white',fontSize:25}}>
                    تخفیف های 
                </Label>
                <Label fontStyle={'Bold'} style={{color:'white',fontSize:25}}>
                    امروز
                </Label>
                </View>
              
                <View style={{ backgroundColor: 'white', borderColor: greenColor, borderWidth: 1,borderRadius:25 }}>
                    <CountDownComponent size={10} until={props.until} />
                </View>

            </Animated.View>
        </View>
    );


}


export default OffList;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        //  marginTop: StatusBar.currentHeight || 0,
    },
    item: {
        backgroundColor: 'white',
        padding: 10,
        paddingBottom: 1,
        marginVertical: 8,
        marginHorizontal: 16,
        width: width * .35,
        height: 230,
        position: 'relative',
        borderRadius: 5

    },
    title: {
        fontSize: 10,
        color: grayColor
    },

    price: {
        fontSize: 10,
        color: 'black'
    },
    offText: {
        fontSize: 8,
        textDecorationLine: 'line-through',
        color: grayColor
    },
    off: {
        borderTopLeftRadius: 0,
        borderBottomLeftRadius: 0,
        borderTopRightRadius: 15,
        borderBottomRightRadius: 15
    }
});
