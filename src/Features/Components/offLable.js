import React from 'react';

import { Text,View } from 'react-native'
import Label from './Label';




const OffLabel=(props)=>{


        return (
          <View style={[{backgroundColor:props.value>=20?'red':'green',borderRadius:5,paddingLeft:5,paddingRight:5,padding:2},props.style]}>
              <Label style={{color:'white',fontSize:12}}>
                    {`${props.value}%تخفیف`}
              </Label>
          </View>
        );
  

}


export default OffLabel;
