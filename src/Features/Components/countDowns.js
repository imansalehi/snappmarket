import React from 'react';
import { Text } from 'react-native'
import CountDown from 'react-native-countdown-component';
import { greenColor, grayColor } from '../../services/utils/theme';



const CountDownComponent = (props) => {


    return (
        <CountDown
            until={props.until}
            size={props.size}
            onFinish={() => null}
            digitStyle={{ backgroundColor: 'transparent' }}
            digitTxtStyle={{ color: greenColor,fontFamily: `IRANSansMobile(FaNum)` }}
            timeLabelStyle={{fontFamily: `IRANSansMobile(FaNum)`,color:greenColor}}
            separatorStyle={{fontFamily: `IRANSansMobile(FaNum)`}}
            timeToShow={['H', 'M', 'S']}
            //showSeparator={true}
            // timeLabels={{m: 'MM', s: 'SS'}}
            timeLabels={{ h: 'ساعت', m: 'دقیقه', s: 'ثانیه' }}
        />
    );


}


export default CountDownComponent;
