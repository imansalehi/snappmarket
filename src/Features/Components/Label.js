import React from 'react';

import { Text } from 'react-native'




const Label=(props)=>{


        return (
            <Text  adjustsFontSizeToFit minimumFontScale={.2} style={[{textAlign:'right',
                color: props.color ? props.color : 'Black', 
                fontFamily: `IRANSansMobile(FaNum)${props.fontStyle ?
                    `_${props.fontStyle}` : ''}`

            },props.style,
         
            ]}>
            
            {props.children}
            
            </Text>
        );
  

}


export default Label;
