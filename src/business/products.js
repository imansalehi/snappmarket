import rest from '../services/external/httpService'


export default class Products {

    constructor(auth) {
        this.rest = new rest();
        this.auth = auth;
    }

  
    getProducts() {
        return new Promise((resolve, reject) => {
            this.rest._send(`/vendors/0jyw5g/main`, undefined, undefined, 'GET').then((res) => {
                resolve(res);
            }).catch((er) => {
                console.log('reject:',er)
                reject(er);
            })
        })
    }

}